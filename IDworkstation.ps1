﻿#* FileName: IDworkstation.ps1
#*=============================================================================
#* Script Name: [ID workstation]
#* Created: [09/02/2013]
#* Author: Damar Johnson
#* Company: Exchange Data International
#* Email: d.johnson@exchange-data.com
#* Web: exchange-data-international
#* Reqrmnts:
#* Keywords:
#*=============================================================================
#* Purpose:  Detects which workstation task file is being executed on, results get appended to OP's index logfile
#*
#*
#*=============================================================================
#*=============================================================================
#* REVISION HISTORY
#*=============================================================================
#* Date: [25/06/2013]
#* Time: [12:00]
#* Issue: Increments aside from webload WCA not being accounted for
#* Solution: Add switches for other increments .i.e SMF
#*
#*=============================================================================
#*=============================================================================
#* FUNCTION LISTINGS
#*=============================================================================
# Function: Get-increment
# Created: [25/06/2013]
# Author: Damar Johnson
# Arguments: Inc, taskfile
# =============================================================================
# Purpose: Determine which incrment to apply
#
#
# =============================================================================
#* FUNCTION LISTINGS
#*=============================================================================
# Function: Set-logpath
# Created: [25/06/2013]
# Author: Damar Johnson
# Arguments: Inc, taskfile
# =============================================================================
# Purpose: Create logpath + logfile
#
#
# =============================================================================

### Logs step and output of entire script
#Start-Transcript O:\AUTO\Scripts\Powershell\Logs\IDworkstation.log -Append

$OFS=''
##Working output directory passed via argument
$ndump=$args[0]
$tail=$args[1]
#$cat=$args[1]

##Testpath
#$ndump='damartest56'
#$tail='i'
#$cat="i"

##Fixed variables
#$logpath="O:\auto\logs\$ndump$inc"
$time=(get-date).hour
$sep='_'

#Get PC name
$SID=$(Get-WmiObject Win32_Computersystem).name

##Date values used
$jdate=get-date -Format yyyyMMdd
$wdate=get-date -Format G
$logvalue="$wdate | Workstation: $SID"

## global variables
#$global:inc
#$global:Fname
#$global:logpath

#$Fname="$jdate$sep$ndump"
#$logpath="O:\auto\logs\$ndump"

#$global:FAname="$jdate,$sep,$ndump,$global:inc"
#$global:FBname=$jdate,$sep,$ndump
#$global:logpathA="\\192.168.2.163\ops\AUTO\logs\$ndump$global:inc"
#$global:logpathB="\\192.168.2.163\ops\AUTO\logs\$ndump"

#If ($tail -eq 'i') {
    ### Output incrment number
    #write-host "Incremental steps applied!";$Fname=$jdate,$sep,$ndump,$inc;$logpath="O:\auto\logs\$ndump$inc"
    #}
    #Else
        #{write-host "Lopsided: procceding with no incrment!";$Fname=$jdate,$sep,$ndump;$logpath="O:\auto\logs\$ndump"}

############################## Determine Increment ##############################
Function Get-Increment {
$time=(get-date).hour
### Webload incrementals
       switch ($ndump) {
       
                ### SMF incrementals values
                'WCAWebload'
                    {switch ($time) {
                        { $_ -ge 7 -and $_ -le 11 } {$global:inc='_1'}
                        { $_ -ge 12 -and $_ -le 16} {$global:inc='_2'}
                        {$_ -ge 17 -and $_ -le 23} {;$global:inc='_3'}
                        default {$global:inc='_0'}
                        }
                    }
                ### SMF incrementals values
                'SMF'
                    {switch ($time) {
                        {$_ -ge 12 -and $_ -le 16} {write-host 'SMF Inc2';$global:inc='_2'}
                        {$_ -ge 17 -and $_ -le 23} {write-host 'SMF Inc3';$global:inc='_3'}
                        default {write-host 'SMF Inc1';$global:inc='_1'}
                            }
                    }
                ### 123Trans incrementals values
                '123Trans' 
                    {switch ($time) {
                        {$_ -ge 12 -and $_ -le 14} {$global:inc='_1'}
                        {$_ -ge 17 -and $_ -le 23} {$global:inc='_2'}
                        default {$global:inc='_1'}
                        }
                    }
                ### CABTrans incrementals values
                'CABTrans' 
                    {switch ($time) {
                        {$_ -ge 11 -and $_ -le 13} {$global:inc='_1'}
                        {$_ -ge 14 -and $_ -le 15} {$global:inc='_2'}
                        {$_ -ge 16 -and $_ -le 17} {$global:inc='_3'}
                        {$_ -ge 18 -and $_ -le 19} {$global:inc='_4'}
                        default {$global:inc='_5'}
                        }
                    }
                ### Default incremental values
                default
                    {switch ($time) {
                        { $_ -ge 12 -and $_ -le 16} {Write-host ' default inc2';$global:inc='_2'}
                        {$_ -ge 17 -and $_ -le 23} {Write-host 'default inc3';$global:inc='_3'}
                        default {Write-host 'default inc1';$global:inc='_1'}
                        }
                    } 
            }
           switch ($tail) {
                       'i' {$global:Fname="$jdate$sep$ndump$inc";$global:logpath="O:\auto\logs\$ndump$inc" ;write-host inc exists!;
                                }
                       '' {write-host "inc is null!! Lopsided: procceding with no incrment!";$global:Fname="$jdate$sep$ndump";$global:logpath="O:\auto\logs\$ndump"}
                       }
}

############################## Determine/Set file log path ##############################        
Function global:Set-Logpath {
### Determine file log path
        IF (!(Test-Path -Path $logpath))
            { write-host No logpath found! Creating Directory; mkdir $logpath }
        Else
            {Write-Host Current logpath exists}    
                ##Create file or append if exist   
                IF (!(Test-Path "$logpath\$Fname.html"))
                { write-host creating file!; ni -path $logpath -Name "$Fname.html" -itemtype "file" -Value "<p><span class='note'>$logvalue</span></p><br/>"}
                Else
                { write-host Appending to file!; Add-Content -Path "$logpath\$Fname.html" -Value "<p><span class='note'>$logvalue</span></p><br/>" -Force}
}

### Execute ###
Get-Increment
Set-Logpath

#Stop-Transcript