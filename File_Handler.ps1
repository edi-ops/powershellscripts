#* FileName: File_Handler.ps1
#*=============================================================================
#* Script Name: [File Handler]
#* Created: [25/06/2013]
#* Author: Damar Johnson
#* Company: Exchange Data International
#* Email: d.johnson@exchange-data.com
#* Web: exchange-data-international
#* Reqrmnts:
#* Keywords:
#*=============================================================================
#* Purpose: All-in-one script for processing any file types
#*
#*
#*=============================================================================
#*=============================================================================
#* REVISION HISTORY
#*=============================================================================
#* Date: [10/09/2013]
#* Time: [12:00]
#* Issue: File in unreadable format when uploaded via FTP.vbs
#* Solution: Force ASCII encoding
#*
#*=============================================================================
#*=============================================================================
#* FUNCTION LISTINGS
#*=============================================================================
# Function: OPS-Copy, OPS-Move, OPS-merge, OPS-Rename, OPS-Tidy, OPS-Zip
# Created: [22/06/2013]
# Author: Damar Johnson
# Arguments: Source, Destination, Filetype, Fileage
# =============================================================================
# Purpose: Copy,Move,Delete,Rename,Zip,Validate files
#
# -a=action, -s=Source, -d=Destination, -t=Type, -i=increment=Y/N, -alert=Y/N, -logfile ops-test, -age=[number of days]
#
#* To Delete -  powershell.exe .\File_Handler.ps1 -a tidy   -s C:\PS\Test -type txt -alert y -logfile ops-test
#* To zip -     powershell.exe .\File_Handler.ps1 -a zip    -s C:\PS\Test -d C:\PS\Test -t csv
#* To Unzip -   powershell.exe .\File_Handler.ps1 -a unzip  -s C:\PS\Test\yyyymmdd.zip -d C:\PS\Test -t csv
#* To Copy -    powershell.exe .\File_Handler.ps1 -a clone  -s C:\PS\Test -d C:\PS\Test2 -t txt
#* To Move -    powershell.exe .\File_Handler.ps1 -a shift  -s C:\PS\Test -d C:\PS\Test2 -d tsk -t txt -i y
#* To Append -  powershell.exe .\File_Handler.ps1 -a append -s C:\PS\Test\File(a) -d C:\PS\Test2\File(b) -type txt
#* To Rename -  powershell.exe .\File_Handler.ps1 -a rename -s C:\PS\Test\File(a) -d C:\PS\Test2\File(b) -type txt
#* To merge -   powershell.exe .\File_Handler.ps1 -a merge  -s C:\PS\Test\File(a) -d C:\PS\Test2\File(b) -type txt 
#* To Validate -   powershell.exe .\File_Handler.ps1 -a val -s C:\PS\Test\File(a) -type txt 
# =============================================================================

#powershell.exe O:\AUTO\Scripts\Powershell\File_Handler_test9.ps1 -a val -s O:\Datafeed\Debt\Markit_JP_Incremental\ -n 56 -alert y
#powershell.exe O:\AUTO\Scripts\Powershell\test\File_Handler_test.ps1 -a unzip -s C:\pstest\datafeed\wcafeed\YYYYMMDD.z03 -d C:\pstest\datafeed\wcafeed\out\ -t eod -alert y

#------------------------- Set script parameters -------------------------#
[CmdletBinding()]
Param(

[Parameter(
Mandatory=$true,Position=0,
ParametersetName='Action'
)]
#[switch]

[Parameter(
#mandatory=$true
)]
[string]$a, #Action to perform

[Parameter(
Mandatory=$true
)]
[string]$s, #Source path of original file

[Parameter(
#Mandatory=$true
)]
[string]$d, #Destination of original file

[parameter(
mandatory=$true
)]
[string]$t, # Type of file 

[parameter(
mandatory=$false
)]
[int]$age='1',

[parameter(
#mandatory=$true
)]
[string]$i, #Increment

[parameter(Mandatory=$false)]
[string]$logfile='OPS-test1',

[parameter(
mandatory=$false
)]
[string]$alert,

[parameter(
mandatory=$false
)]
[int]$n


)

#------------------------- Set alert function -------------------------#

## MessageBox
[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null

Function Msgbox() {

param(
[string]$stop, #Msgbox popup
[string]$global:label='Error'
)

    switch ($alert){
    'y' {[System.Windows.Forms.MessageBox]::Show("*[$action] $stop","$global:label`: "+"$logfile")}
    default {break}
    }
}

#------------------------- DB Logging -------------------------#
## Set MySQL Connectiion
[void][system.reflection.assembly]::LoadFrom("C:\Program Files (x86)\MySQL\MySQL Connector Net 6.6.5\Assemblies\v2.0\MySql.Data.dll")
$myconnection=New-Object MySql.Data.MySqlClient.MySqlConnection

$server='192.168.2.60'
$id='sa'
$pwd='K376:lcnb'
$db='AutoOps'
$tbl='opslog'

#------------------------- DB connections -------------------------#
## Default
$myconnection.Connectionstring="server=$server;database='AutoOps';Persist Security Info=false;user id=$id;pwd=$pwd"

$myconnection.open()
#------------------------- Queries -------------------------#
#$WCA="SELECT feeddate FROM wca.tbl_opslog order by acttime asc;"
#$run="INSERT INTO opslog (taskfile,lpath,file,status,bytesize,seq) VALUES ('"+$logfile+"','"+$fhome+"','"+$found+"','"+$Global:class+"','"+$size+"','"+$global:inc+"')"
$command=$myconnection.CreateCommand()
$command.CommandText=$run
#------------------------- Output Results -------------------------#
#$reader=$Command.ExecuteReader()
#$dataset=New-Object System.Data.DataSet
#$dataset.Tables[0]
#$myconnection.Close()
#------------------------- Static variables -------------------------#
$action=$a
$source=$s
$dest=$d #| ?{$_ -replace('YYYYMMDD',$jdate)}
$type=$t
#$age {$age}
 
$time=(get-date).hour
$jdate=get-date -Format yyyyMMdd
$ddate=(get-date).tostring("yyyy-MM-dd")

$ddateMM=(get-date).tostring("yyyy-MM")

$wdate=get-date -Format G
$sep='_'
#$global:logfile='zone'
$global:class='ok'

$global:logvalue=@()
$global:source=$source

#$source="'$source'"
    switch ($source){
        {$_.Contains("+")}{$source="$source";$source.Split("+") | tee -Variable section}
        default{"Single File, Nothing Done!!"} 
    }

#switch ($blanco){
    #{'YYYYMMDD'}{}
    #}
#$dest | ?{$_ -replace('YYYYMMDD',$jdate)}

#------------------------- Determine INC and create logpath -------------------------#
Function Get-Increment {
       switch ($logfile) {
       
                #------------------------- WCA incrementals values -------------------------#
                'WCAWebload'
                    {switch ($time) {
                        { $_ -ge 12 -and $_ -le 16} {$global:inc='_2'}
                        {$_ -ge 17 -and $_ -le 23} {;$global:inc='_3'}
                        default {$global:inc='_1'}
                        }
                    }
                
                #------------------------- SMF incrementals values -------------------------#
                'SMF'
                    {switch ($time) {
                        {$_ -ge 12 -and $_ -le 16} {write-host 'SMF Inc2';$global:inc='_2'}
                        {$_ -ge 17 -and $_ -le 23} {write-host 'SMF Inc3';$global:inc='_3'}
                        default {write-host 'SMF Inc1';$global:inc='_1'}
                            }
                    }
                #------------------------- 123Trans incrementals values -------------------------#
                '123Trans' 
                    {switch ($time) {
                        {$_ -ge 12 -and $_ -le 14} {$global:inc='_1'}
                        {$_ -ge 17 -and $_ -le 23} {$global:inc='_2'}
                        default {$global:inc='_1'}
                        }
                    }
                #------------------------- CABTrans incrementals values -------------------------#
                'CABTrans' 
                    {switch ($time) {
                        {$_ -ge 11 -and $_ -le 13} {$global:inc='_1'}
                        {$_ -ge 14 -and $_ -le 15} {$global:inc='_2'}
                        {$_ -ge 16 -and $_ -le 17} {$global:inc='_3'}
                        {$_ -ge 18 -and $_ -le 19} {$global:inc='_4'}
                        default {$global:inc='_5'}
                        }
                    }
                #------------------------- CABTrans incrementals values -------------------------#
                't15022_inc' 
                #'damar-test'
                    {
                        switch ($time) {
                            { $_ -ge 13 -and $_ -le 16} {$global:inc='_2';$global:fileinc='153000'}
                            {$_ -ge 17 -and $_ -le 23} {$global:inc='_3';$global:fileinc='203000'}
                            default {$global:inc='_1';$global:fileinc='083000'}
                                }
                                $global:source=$source.replace('????????',$jdate)#.replace('??????',$global:fileinc)
                                $global:dest=$dest.replace('????????',$jdate).replace('??????',$global:fileinc)
                            }
                #------------------------- t15022 incrementals values -------------------------#
                'damar-test'
                    {
                        switch ($time) {
                            { $_ -ge 13 -and $_ -le 16} {$global:inc='_2';$global:fileinc='153000'}
                            {$_ -ge 17 -and $_ -le 23} {$global:inc='_3';$global:fileinc='203000'}
                            default {$global:inc='_1';$global:fileinc='083000'}
                                }
                                $global:source=$source.replace('????????',$jdate)#.replace('??????',$global:fileinc)
                                $global:dest=$dest.replace('????????',$jdate).replace('??????',$global:fileinc)
                            }
                #'t15022_inc' 
                    #{
                        #switch ($time) {
                            #{ $_ -ge 13 -and $_ -le 17} {$global:inc='_2';$global:fileinc='153000'}
                            #{$_ -ge 18 -and $_ -le 23} {$global:inc='_3';$global:fileinc='203000'}
                            #default {$global:inc='_1';$global:fileinc='083000'}
                                #}
                                #$global:source=$source.replace('????????',$jdate)#.replace('??????',$global:fileinc)
                                #$global:dest=$dest.replace('????????',$jdate).replace('??????',$global:fileinc)
                        #}
                            #}
                #------------------------- CABTrans incrementals values -------------------------#
                'xdes' 
                    {switch ($time) {
                        {$_ -ge 11 -and $_ -le 13} {$global:inc='_1';$global:fileinc='07'}
                        {$_ -ge 14 -and $_ -le 15} {$global:inc='_2';$global:fileinc='10'}
                        {$_ -ge 16 -and $_ -le 17} {$global:inc='_3';$global:fileinc='15'}
                        default {$global:inc='_1';$globalfile:inc='07'}
                        }
                    }
                #------------------------- Default incremental values -------------------------#
                default
                    {switch ($time) {
                        { $_ -ge 13 -and $_ -le 16} {Write-host ' default inc2';$global:inc='_2'}
                        {$_ -ge 17 -and $_ -le 23} {Write-host 'default inc3';$global:inc='_3'}
                        default {Write-host 'default inc1';$global:inc='_1'}
                        }
                        #$dest | ?{$_ -replace('YYYYMMDD',$jdate)}
                        #$dest | ?{$_ -replace('YYYY-MM-DD',$ddate)}
                    } 
            }
           switch ($i) {
                       'y' {$global:Fname="$jdate$sep$logfile$inc";$global:logpath="O:\auto\logs\$logfile$inc";$global:outfile='y' ;write-host inc exists!;
                            }
                       default {write-host "inc is null!! Lopsided: procceding with no incrment!";$global:Fname="$jdate$sep$logfile";$global:logpath="O:\auto\logs\$logfile"} #"$logfile"}
                       }
}

#------------------------- Feed Date -------------------------#
#switch($source){
    #{$_.endswith("YYYYMMDD")}{$source=$source.replace('YYYYMMDD',$jdate)}
    #{$_.startswith("YYYYMMDD")}{$source=$source.replace('YYYYMMDD',$jdate)}
    #{$_ -like "*YYYYMMDD*"}{$source=$source.replace('YYYYMMDD',$jdate)}
    #{$_.endswith("YYYY-MM-DD")}{$source=$source.replace('YYYY-MM-DD',$ddate)}
    #{$_.startswith("YYYY-MM-DD")}{$source=$source.replace('YYYY-MM-DD',$ddate)}
    #{$_ -like "*YYYY-MM-DD*"}{$source=$source.replace('YYYY-MM-DD',$ddate)}
        #Default{$source=$source}    
            #}

#switch($dest){
    #{$_.endswith("YYYYMMDD")}{$dest=$dest.replace('YYYYMMDD',$jdate)}
    #{$_.startswith("YYYYMMDD")}{$dest=$dest.replace('YYYYMMDD',$jdate)}
    #{$_ -like "*YYYYMMDD*"}{$dest=$dest.replace('YYYYMMDD',$jdate)}
    #{$_.endswith("YYYY-MM-DD")}{$dest=$dest.replace('YYYY-MM-DD',$ddate)}
    #{$_.startswith("YYYY-MM-DD")}{$dest=$dest.replace('YYYY-MM-DD',$ddate)}
    #{$_ -like "*YYYY-MM-DD*"}{$dest=$dest.replace('YYYY-MM-DD',$ddate)}
        #Default{$dest=$dest}
            #}
switch($global:source){
    {$_ -eq $null}{$global:source=$source}
    Default{$global:source=$global:source}
        }

switch($global:dest){
    {$_ -eq $null}{$global:dest=$dest}
    Default{$global:dest=$global:dest}
        }

#------------------------- OP's logging -------------------------#
Function Set-Logpath {
#****Determine file log path*******
        IF (!(Test-Path -Path $logpath))
            {write-host No logpath found! Creating Directory; mkdir $logpath
                }
        Else
            {Write-Host Current logpath exists
                }    
                ##Create file or append if exist   
                IF (!(Test-Path "$logpath\$Fname.html"))
                    {write-host creating file!; ni -path $logpath -Name "$Fname.html" -itemtype "file" -Value "<link rel=stylesheet href=../style.css /><p><span class='$global:class'>$global:logvalue</span></p>"
                        }
                Else
                    #{ write-host Appending to file B!; ac "$logpath\$Fname.html" -Value "<p><span class='$class'>$global:logvalue</span></p>" -Force}
                    #{ write-host Appending to file B!; ac "$logpath\$Fname.html" -Value "<p><p><span class='$global:noteclass'>$global:note</span></p><span class='$global:class'>$global:logvalue</span></p>" -Force
                    { write-host Appending to file B!; ac "$logpath\$Fname.html" -Value "<p><span class='$global:class'>$global:logvalue</span></p>" -Force
                        }
}

#------------------------- Logging FTP Function -------------------------#
    function LogThis()
    {
    param(
    [string]$displaytxt
    )
        #$hold=@()
        #$global:thestr = $args[0]
        $d = (Get-Date).tostring("yyyy-MM-dd HH:mm:ss")
        "$wdate | $displaytxt" | tee -Variable global:logvalue #>> 'c:\PS\tekken.txt' #tee -Variable global:logvalue >> 'c:\PS\tekken.txt'
        #"$wdate | $thestr" | tee -Variable global:logvalue
        write-output "$d $displaytxt" #;exit
        #$hold.count
        $global:logvalue
        #if ($args[1] -eq "exit") {exit}
    }

#Start-Transcript O:\AUTO\Scripts\Powershell\Logs\File_Handler.log -Append

#------------------------- Set file age -------------------------#

#function Age ($age='0') { 
function Age { 
    switch ($age){
        '0'{$input |?{$_.LastWriteTime.Date -eq (get-date).Date.AddDays($age)}}
        default{$input |?{$_.LastWriteTime.Date -le (get-date).Date.AddDays($age)}}
        #$input |?{$_.LastWriteTime.Date -lt (get-date).Date.AddDays($age)}
        #$input |?{$_.LastWriteTime.Date -eq (get-date).Date.AddDays($age)}
        #$_ |?{$_.LastWriteTime.Date -eq (get-date).Date.AddDays($age)}
        #$item |?{$_.LastWriteTime.Date -eq (get-date).Date.AddDays($age)}
    }
}

#------------------------- Copy func -------------------------#
    Function OPS-Copy { #($type='zip') {
    $x=Test-Path $source
        switch ($x) {
                'True'
                    {switch ($a){
                    {($source | split-path -leaf).contains('.') -eq 'True'}{cd (split-path -parent $source)}
                    default{cd $source}
                    }
    $y=Test-Path $dest
                    switch ($y){
                            'True' 
                                {switch ($x){
                                #(!(test-path "$source\*.$type")){
                                (!(test-path $source -Filter *.$type)){
                                    LogThis -displaytxt "[$action] $source`: $type`: File type specified does not exist" #Write-Host 'File type specified does not exist'
                                        msgbox "File type specified does not exist"
                                        $global:class='failed';Get-Increment;Set-Logpath
                                            }
                                default {
                                    switch ($x){
                                        #$source.Contains('.') {gci ((split-path -leaf $source) | tee -Variable item | age) -Filter "*.$type" | cpi -Destination $dest}
                                        #$source.Contains('.') {$get_source= gci((split-path -leaf $source) | tee -Variable item | age)
                                        #$source.Contains('.') {$get_source= gci (split-path -leaf $source) | tee -Variable item;$item| age
                                        $source.Contains('.') {$get_source= gci (split-path -leaf $source) | tee -Variable item; $get_source=($get_source| age)
                                        $get_source | % -Begin {cls;$ii=0} -Process {$x=$_; $ii=$ii+1;$record=$get_source.count
                                            Write-Progress -Activity "$action File(s)" -status "Processing $ii of $record $type file(s)" -PercentComplete ($ii/$get_source.count*100) `
                                            -CurrentOperation "=> $x";cpi $x -Destination $dest
                                                }
                                            }
                                        #default {cpi (gci $source -Filter "*.$type" | tee -Variable item | age) -Destination $dest}
                                        #default {$get_source= (gci $source -Filter "*.$type" | tee -Variable item | age)
                                        #default {$get_source= ((gci $source -Filter "*.$type") | tee -Variable item ($_| age))
                                        default {$get_source= (gci $source -Filter "*.$type") | tee -Variable item; $get_source=($get_source| age)
                                        $get_source | % -Begin {cls;$ii=0} -Process {$x=$_; $ii=$ii+1;$record=$get_source.count
                                            Write-Progress -Activity "$action File(s)" -status "Processing $ii of $record $type file(s)" -PercentComplete ($ii/$get_source.count*100) `
                                            -CurrentOperation "=> $x";cpi $x -Destination $dest
                                                }
                                            #Write-Progress -Id 2 -Activity "$action File(s)" -status 'Curent Job'-PercentComplete ($j/$get_source.count*100) `
                                            #-CurrentOperation progress
                                                }
                                            }
                                            #gci ((split-path $source) | tee -Variable item | age) -Filter "*.$type" | cpi -Destination $dest
                                            #cpi (gci (split-path $source) -Filter "*.$type" | tee -Variable item | Age)  $dest -Force
                                            $total=$item.Count;Get-Increment
                                            Logthis -displaytxt "$total`: File(s) successfully copied! | Source:$source";Set-Logpath
                                                foreach ($x in $item){
                                                switch ($total){
                                                    #'1'{logthis "$x deleted";break}
                                                    #{$total -gt '1'}{write-host "$total";logthis "$x copied";$global:class='Report';$count+++1 | tee -Variable list;Write-Host "$list";Set-Logpath
                                                    {$total -gt '1'}{write-host "$total" | Out-Null;logthis "Copy $x => $dest";$global:class='Report';$count+++1 | tee -Variable list;Write-Host "$list" | Out-Null;Set-Logpath
                                                        switch ($list){
                                                            {$list -ge $total}{exit $LASTEXITCODE}
                                                            #default {logthis "$x copied"}#;break}
                                                            default {logthis "Copy- $global:source => $global:dest";$global:class='Report';}#Get-Increment;Set-Logpath}#;break}
                                                            #default {logthis "Copy- $x => $global:dest";$global:class='Report';Get-Increment;Set-Logpath}#;break}
                                                                }
                                                        }
                                                    default {logthis "Copy - $x => $global:dest";$global:class='Report';Get-Increment;Set-Logpath}#;break}
                                                    #default {logthis "$x copied"}
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    'False' 
                                        {logthis -displaytxt "[$action] $dest`: Destination path specified does not exist please re-check path!"
                                            msgbox "Destination path specified does not exist please re-check path!"
                                            $global:class='failed';Get-Increment;Set-Logpath
                                            }
                                }
                            }
                        'False' {logthis -displaytxt "[$action] $source`: Source path specified does not exist please re-check path!"
                                    msgbox "Source Path does not exist please re-check path!"
                                    $global:class='failed';Get-Increment;Set-Logpath;break
                                    }
                }
            }
 #------------------------- Append func -------------------------#

    Function OPS-Append {
    $x=Test-Path $source
        switch ($x){
            'True'
                {$y=Test-Path $dest
                switch ($y){
                            'True' 
                                {cat $source | ac $dest -Force;logthis -displaytxt "[$action] File successfully ammended!";Get-Increment;Set-Logpath
                                 logthis "Ammend- $global:source => $global:dest"
                                 #logthis "Source $source".replace('????????',$jdate).Replace('??????',$fileinc),
                                    #Target:,"$dest".replace('????????',$jdate).replace('??????',$fileinc)
                                    }   
                            'False'
                                {logthis -displaytxt "[$action] $dest`: End File to be ammended to does not exist please re-check end file!"
                                    msgbox "End File to be ammended to does not exist please re-check end file!"
                                    $global:class='failed';Get-Increment;Set-Logpath;break
                                }
                            }
                }
            'False' 
                {logthis -displaytxt "[$action] $source`: Source file does not exist please re-check source path!"
                    msgbox "Source file does not exist please re-check source file!"
                    $global:class='failed';Get-Increment;Set-Logpath;break
                    }
            }
}

 #------------------------- Merge func -------------------------#

    Function OPS-15022-Merge {
    Get-Increment
    #$mtest=(split-path -Parent $dest)
    #$childa=gci (split-path -Parent $source)
    #$childb=gci (cd(split-path -leaf $dest))
    #$source="'$source'"
    #switch ($source){
        #{$_.Contains(",")}{$source.Split(",") | tee -Variable section}
    #}
    write-host $section
    #switch ($section){
        #{$_ -eq $null}{
        #$xsource=Test-Path $source
        #switch ($xsource){
            #'True'
                #{switch ($nsource){
                    #{$source.contains('[a-z].')}{cd (split-path -parent $source)
                    #{($source | split-path -Leaf) -match '[.]'}{cd (split-path -parent $source)
                            #}
                    #default{cd $source}
                        #}
                #$ydest=Test-Path $dest;$dest
                $xsource=Test-Path $source
                switch ($xsource){
                        'True' 
                            #{if ($source.Contains('.')){split-path -parent $global:source}
                                {gci $source | ?{$_.Name -match 'edi_[0-9]*_[0-9]*.txt' -or $_.Name -match '15022_MT568_EOD.txt'} | tee -variable item | gc | tee -variable mval; $mval | Out-File $global:dest -Encoding ASCII
                                #{gci (split-path -parent $global:source) | ?{$_.Name -match 'edi_\p{Nd}+?_\p{Nd}+?.txt'} | tee -variable item | gc | tee -variable mval; $mval | Out-File $global:dest
                                #{gci (split-path -parent $global:source) | ?{$_.Name -match 'edi_[0-9]*_[0-9]*.txt'} | tee -variable item | gc | tee -variable mval; $mval | Out-File $global:dest
                                #ni -Path (split-path -Parent $dest) -Name (split-path -Leaf $source) -ItemType 'file' -Value "$mval"
                                $total=$item.Count;$mval.count;Get-Increment
                                Logthis -displaytxt "$total`: File(s) successfully merged!";$global:class='ok';Set-Logpath;foreach ($x in $item)
                                {
                                                switch ($total){
                                                    #'1'{logthis "$x deleted";break}
                                                    {$total -ge '1'}{write-host "$total";logthis "Merge- $x => $global:dest";$count+++1 | tee -Variable list;Write-Host "$list";$global:class='Report';Set-Logpath
                                                        switch ($list){
                                                            {$list -ge $total}{exit $LASTEXITCODE}
                                                            #default {logthis "$x copied"}#;break}
                                                            default {logthis "Merge- $x => $global:dest";$global:class='Report';Get-Increment;Set-Logpath;break
                                                                }#;break}
                                                            }
                                                        }
                                                    }
                                                }
                                    }   
                            'False'
                                {logthis -displaytxt "[$action] $dest`: End File to be merged to does not exist please re-check end file!"
                                    msgbox "End File to be merged to does not exist please re-check end file!"
                                    $global:class='failed';Get-Increment;Set-Logpath;break
                                }
                            }
                #}
            'False' 
                {logthis -displaytxt "[$action] $source`: Source file does not exist please re-check source path!"
                    msgbox "Source file does not exist please re-check source file!"
                    $global:class='failed';Get-Increment;Set-Logpath;break
                    }
            #}
    #}
        #default{
        #$x=Test-Path $section
                    switch ($x){
                        'True'
                            {switch ($a){
                                {(split-path $section -Leaf).StartsWith('edi')}{cd (split-path -Parent $section[0])}
                                #$source.Contains('edi_') {cd (split-path -Parent $source)}
                                default {$section} #cd $section}
                                }
                                    $y=Test-Path (split-path -Parent $global:dest)
                                    switch ($y){
                                            'True' 
                                                #{if ($source.Contains('.')){split-path -parent $global:source}
                                                    {$global:dest; $files=gci $section | ?{$_.Mode -eq '-a---'};
                                                    $files | ?{$_.Name -match 'edi_[0-9]*_[0-9]*.txt' -or $_.Name -match '15022_EQ568_EOD.txt' -or 'canc.txt'} | 
                                                    tee -variable item | gc | tee -variable mval; $mval | Out-File $global:dest -Encoding ASCII -Force
                                                    #{gci (split-path -parent $global:source) | ?{$_.Name -match 'edi_\p{Nd}+?_\p{Nd}+?.txt'} | tee -variable item | gc | tee -variable mval; $mval | Out-File $global:dest
                                                    #{gci (split-path -parent $global:source) | ?{$_.Name -match 'edi_[0-9]*_[0-9]*.txt'} | tee -variable item | gc | tee -variable mval; $mval | Out-File $global:dest
                                                    #ni -Path (split-path -Parent $dest) -Name (split-path -Leaf $source) -ItemType 'file' -Value "$mval"
                                                    $total=$item.Count;$mval.count;Get-Increment
                                                    Logthis -displaytxt "$total`: File(s) successfully merged!";$global:class='ok';Set-Logpath
                                                    foreach ($x in $item){
                                                                    switch ($total){
                                                                        #'1'{logthis "$x deleted";break}
                                                                        {$total -gt '1'}{write-host "$total";logthis "Merge- $x => $global:dest";$count+++1 | tee -Variable list;Write-Host "$list";Set-Logpath
                                                                            switch ($list){
                                                                                {$list -ge $total}{exit $LASTEXITCODE}
                                                                                #default {logthis "$x copied"}#;break}
                                                                                default {logthis "Merge- $x => $global:dest"
                                                                                    }#;break}
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                        }   
                                                'False'
                                                    {logthis -displaytxt "[$action] $dest`: End File to be merged to does not exist please re-check end file!"
                                                        msgbox "End File to be merged to does not exist please re-check end file!"
                                                        $global:class='failed';Get-Increment;Set-Logpath
                                                    }
                                                }
                                    }
                                'False' 
                                    {logthis -displaytxt "[$action] $source`: Source file does not exist please re-check source path!"
                                        msgbox "Source file does not exist please re-check source file!"
                                        $global:class='failed';Get-Increment;Set-Logpath;break
                                        }
                                }
                        #}
        #}
  #}
}
#------------------------- Move func -------------------------#
    Function OPS-Move { #($type='*') {
    $xsource=Test-Path $source
        switch ($xsource){
            'True'
                {switch ($nsource){
                    #{$source.contains('[a-z].')}{cd (split-path -parent $source)
                    {($source | split-path -Leaf) -match '[.]'}{cd (split-path -parent $source)
                            }
                    default{cd $source}
                        }
                $ydest=Test-Path $dest;$dest
                switch ($ydest){
                    'True' 
                        #{switch ($x){
                        #{(!(test-path $source -Filter *.$type))}{LogThis -displaytxt "[$action] $source`: $type`
                            #: File type specified does not exist";msgbox "File type specified does not exist";$global:class='failed'}
                        #default {
                            {switch ($xsource){
                                #$source.Contains('.') {gci ((split-path -leaf $source) | tee -Variable item | age) -Filter "*.$type" | cpi -Destination $dest}
                                $source.Contains('[a-z].') {gci ((split-path $source) | tee -Variable item | age) `
                                    $item | % -Begin {$ii=0} -Process {$xx=$_;$ii=$ii+1;$record=$item.count
                                    #Write-Progress -Activity "$action File(s)" -status "Processing $i of $record $type file(s)" -PercentComplete ($i/$get_source.count*100) `
                                    #-CurrentOperation "=> $x";
                                    #mi $x -Destination $dest
                                    mi $item -Destination $dest
                                        }
                                    }
                                #default {cpi (gci $source -Filter "*.$type" | tee -Variable item | age) -Destination $dest}
                                default {(gci $source -Filter "*.$type" | tee -Variable item | age)
                                    $item | % -Begin {$ii=0} -Process {$xx=$_;$ii=$ii+1;$record=$get_source.count
                                    #Write-Progress -Activity "$action File(s)" -status "Processing $i of $record $type file(s)" -PercentComplete ($i/$get_source.count*100) `
                                    #-CurrentOperation "=> $x";
                                    #mi $x -Destination $dest
                                    mi $item -Destination $dest
                                        #}
                                    #}
                                #Write-Progress -Id 2 -Activity "$action File(s)" -status 'Curent Job'-PercentComplete ($j/$get_source.count*100) `
                                #-CurrentOperation progress
                                        #}
                                    #}
                            #mi ((gci $source -Filter "*.$type" | tee -Variable item) | Age) $dest -Force
                                $total=$item.Count;Get-Increment
                                logthis "$total`: File(s) successfully moved! | Source:$Source";Set-Logpath
                                    foreach ($x in $item){
                                    switch ($total){
                                        {$total -gt '1'}{logthis "Move $x => $dest";$count+++1 | tee -Variable list ;write-host $list | Out-Null;$global:class='Report';Set-Logpath
                                            switch ($list){
                                                {$list -ge $total}{exit $LASTEXITCODE}
                                                #default {logthis "$x moved";break}
                                                default {logthis "Move $x => $dest";$global:class='Report';Get-Increment;Set-Logpath}
                                                    }
                                                }
                                            #default {logthis "$x moved";break}
                                            default {write-host | Out-Null;$dest;logthis "Move $x => $dest";$global:class='Report';Get-Increment;Set-Logpath}
                                        }
                                    }
                            }
                            'False'
                                {logthis -displaytxt "[$action] $source`: File(s) to be moved does not exist please re-check file/path!"
                                    msgbox "File(s) to be moved does not exist please re-check file/path!"
                                    $global:class='failed';Get-Increment;Set-Logpath
                                }
                            }
                }
            'False' 
                {logthis -displaytxt "[$action] $source`: Source file path specified does not exist please re-check file/path!"
                    msgbox "Source file path does not exist please re-check file/path!"
                        $global:class='failed';Get-Increment;Set-Logpath
                            }
                    }
                }
            }
    }
 }           
###########------------------------- Rename func -------------------------#
    Function OPS-Rename { 
    $x=Test-Path $source
        switch ($x) {
                'True'
                    {switch ($a){
                    {$source.contains('.')} {cd (split-path -parent $source)
                        }
                    default{cd $source}
                    }
                    $y=Test-Path (split-path -Parent $dest)
                    switch ($y){
                            'True' 
                                {switch ($x){
                                #(!(test-path "$source\*.$type")){
                                (!(test-path $source -Filter *.$type)){
                                    LogThis -displaytxt "[$action] $source`: $type`: File type specified does not exist" #Write-Host 'File type specified does not exist'
                                        msgbox "File type specified does not exist"
                                        $global:class='failed';Get-Increment;Set-Logpath
                                            }
                                default {
                                    switch ($x){
                                        $source.Contains('.') {gci ((split-path -leaf $source) | tee -Variable item | age) -Filter "*.$type" | cpi -Destination (Split-Path -Parent $dest)
                                        cd (Split-Path -Parent $dest);Write-Host ((Split-Path -Parent $dest)+'\'+(split-path -leaf $source)) | Out-Null; rni -Path ((Split-Path -Parent $dest)+'\'+(split-path -leaf $source)) -NewName (split-path -Leaf $dest)}
                                        default {cpi (gci $source -Filter "*.$type" | tee -Variable item | age) -Destination $dest}
                                                }
                                            $total=$item.Count;Get-Increment
                                            Logthis -displaytxt "$total`: File(s) successfully Renamed!";Set-Logpath
                                                foreach ($x in $item){
                                                switch ($total){
                                                    {$total -gt '1'}{write-host "$total";logthis "$x renamed";$count+++1 | tee -Variable list;Write-Host "$list";Set-Logpath
                                                        switch ($list){
                                                            {$list -ge $total}{exit $LASTEXITCODE}
                                                            default {logthis "Rename- $global:source => $global:dest"}#;break}
                                                                }
                                                        }
                                                    default {logthis "Rename - $global:source => $global:dest";Get-Increment;Set-Logpath;break}
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    'False' 
                                        {logthis -displaytxt "[$action] $dest`: Destination path specified does not exist please re-check path!"
                                            msgbox "Destination path specified does not exist please re-check path!"
                                            $global:class='failed';Get-Increment;Set-Logpath
                                            }
                                }
                            }
                        'False' {logthis -displaytxt "[$action] $source`: Source path specified does not exist please re-check path!"
                                    msgbox "Source Path does not exist please re-check path!"
                                    $global:class='failed';Get-Increment;Set-Logpath
                                    }
                }
            }

#------------------------- Delete func -------------------------#

    Function OPS-Tidy { #($type='*') {
    $x=Test-Path $source
        switch ($x) {
            'True'{
                cd $source
                switch ($type){
                {(!(test-path "$source\*.$type"))}{
                    LogThis -displaytxt "*[$action] $source- $type`: File type specified does not exist" #Write-Host 'File type specified does not exist
                    msgbox "File specified does not exist `n`n Please Check Logs for Details!"
                    $global:class='failed';Get-Increment;Set-Logpath
                            }
                        default {
                            #ri ((gci $source -Filter "*.$type" | tee -Variable item) | Age) -Force
                            #$get_source= (gci $source -Filter "*.$type" | tee -Variable item | age)
                            $get_source= (gci $source -Filter "*.$type") | tee -Variable item; $get_source=($get_source| age)
                            $get_source | % -Begin {$ii=0} -Process {$x=$_; $ii=$ii+1;$record=$get_source.count
                                        Write-Progress -Activity "$action File(s)" -status "Processing $ii of $record $type file(s)" -PercentComplete ($ii/$get_source.count*100) `
                                        -CurrentOperation "=> $x";ri $x
                                            }
                            #ri "$source\*.$type" | tee -Variable item | Age -Force
                            $total=$item.count;get-increment
                            logthis "$total`: File(s) successfully deleted! | Source:$source";Set-Logpath
                            foreach ($x in $item){
                                switch ($total){
                                    #'1'{logthis "$x deleted";break}
                                    {$total -gt '1'}{logthis "$x deleted";$count+++1 | tee -Variable list ;$global:class='Report';write-host $list | Out-Null;#Set-Logpath
                                        switch ($total){
                                                {$list -ge $total}{set-logpath;break}#exit $LASTEXITCODE}
                                                default {logthis "$x deleted";$global:class='Report';Set-Logpath;break}
                                                }
                                            }
                                    default {logthis "$x deleted";$global:class='Report';Set-Logpath;break}
                                    }
                                }
                            }
                        }
                }
            'False' {
                    Logthis -displaytxt "[$action] $source`: Source file path specified does not exist please re-check file/path!"
                        msgbox "Source file path does not exist please re-check file/path!"
                        $global:class='failed';Get-Increment;Set-Logpath
                            }
                        }
            }
        
#------------------------- Zip func -------------------------#
    Function OPS-Zip { #($type='*'){
    Get-Increment
    $app="$env:ProgramFiles\7-Zip\7z.exe"
        switch (test-path $app){
            {'True'}{Set-Alias 7zip "$env:ProgramFiles\7-Zip\7z.exe"}
            default {LogThis "Application not found: $app"
                    Msgbox "Application not found: $app"
                    }
                }
        switch ($source){
            {(!(Test-Path $source))}{
                LogThis -displaytxt "[$action] $source`: Source file path specified does not exist please re-check file/path!"
                Msgbox "File path specified does not exist please re-check file/path!"
                $global:class='failed';Get-Increment;Set-Logpath
                    }
            default {
                cd $source
                #$file=(gci $source | ?{$_.extension -like "*.$type"} | Age) | ?{$_.mode -like '-a---'} | tee -Variable item;$file #>> C:\PS\Test\test_exist.txt
                $file=(gci $source | ?{$_.extension -like "*.$type"} | ?{$_.mode -like '-a---'}) | tee -Variable item; $file=($file| age);$file
                $file | % -Begin {cls;$ii=0} -Process {$x=$_; $ii=$ii+1;$record=$file.count
                Write-Progress -Activity "$action File(s)" -status "Processing $i of $record $type file(s)" -PercentComplete ($ii/$file.count*100) `
                -CurrentOperation "=> $x"#;ri $x
                    }
                $total=$item.Count;#Write-Host item=$item.Count
        switch ($x){
            {(!(test-path "$source\*.$type"))}{
                LogThis -displaytxt "[$action] $type`:  File type specified does not exist" #| TF: $logfile"
                msgbox "File type specified does not exist" #| TF: $logfile"  
                $global:class='failed';Get-Increment;Set-Logpath
                        }
            default{
                7zip a "-tzip" $archive,$file "*.$type" #$source,$dest
                LogThis -displaytxt "$total`: File(s) successfully zipped! | Archive:$Source\$archive.zip";Set-Logpath;
                foreach ($x in $item){
                    switch ($total){
                        {$total -gt '1'}{logthis "$x zipped";$count+++1 | tee -Variable list ;write-host list=$list | Out-Null;Write-Host item=($item.Count) | Out-Null;$global:class='Report' ;Set-Logpath
                            switch ($list){
                                {$list -ge $total}{exit $LASTEXITCODE}
                                default {logthis "$x zipped";$global:class='Report';Get-Increment;Set-Logpath;break}
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
#-------------------------Un-Zip func -------------------------#
    Function OPS-Unzip { #($type='*'){
    Get-Increment
    $app="$env:ProgramFiles\7-Zip\7z.exe"
        switch (test-path $app){
        {'True'}{Set-Alias 7zip "$env:ProgramFiles\7-Zip\7z.exe"}
        default {LogThis "Application not found: $app"
                    Msgbox "Application not found: $app"
                    }
                }
    switch ($dest){
        {(!(Test-Path $dest))}{
            LogThis -displaytxt "[$action] $dest`: Source file path specified does not exist please re-check file/path!"
            Msgbox "File path specified does not exist please re-check file/path!"
            $global:class='failed';Get-Increment;Set-Logpath;break}
        default {
            #cd $dest
            $zipdest='-o'+$dest}
    }
    switch ($archive){
        {(!(test-path $archive))}{
        LogThis -displaytxt "[$action] $archive`: Archive does not exist please re-check file/path!"
            Msgbox "Archive does not exist please re-check file/path!"
            $global:class='failed';Get-Increment;Set-Logpath;break}
        Default {LogThis -displaytxt "[$action] $archive`: Archive Found!";
            $global:class='ok';Get-Increment;Set-Logpath;
            $file=$archive | Split-Path -Leaf;$parent=$archive | Split-Path -parent
            $item=$file.count
            $total=$item;#Write-Host item=$item.Count
                    7zip e $archive $zipdest "*.$type" #$source,$dest
                    LogThis -displaytxt "$total`: File(s) successfully unzipped! | Archive:$archive";Set-Logpath;
                    foreach ($x in $item){
                        switch ($total){
                            {$total -ge '1'}{logthis "$file unzipped => $dest";$count+++1 | tee -Variable list ;write-host list=$list | Out-Null;Write-Host item=($item.Count) | Out-Null;$global:class='Report' ;Set-Logpath
                                switch ($list){
                                    {$list -ge $total}{break;exit $LASTEXITCODE}
                                    default {logthis "$file unzipped => $dest";$global:class='Report';Get-Increment;Set-Logpath;break}
                                        }
                                    }
                                }
                            }
                        }
            }
        }
        

#------------------------- File Validation func -------------------------#
    Function OPS-Val { #($type='*') {
#if ($source.Contains('[a-z].')){
$single=$source | Split-Path -Leaf
$fhome=$source | Split-Path -Parent
$fhome=$fhome | %{$_.replace('\','/')}
if ($single -like '*.*'){
    $x=Test-Path ($source | split-path -Parent)
    $file=($source | Split-Path -Leaf) 
    $total=$item.count; LogThis "$total`: File(s) found | Source:$source";$global:class='ok';#Get-Increment;Set-Logpath
    switch($x){
        'False'{'Source path not found!';$global:class='failed';
            Msgbox 'Source path not found!'
                Get-Increment;set-logpath}
         default{$col=gci ($source | Split-Path -Parent) | ?{$_.mode -like '-a---'} | ?{$_.extension -like "*.$type"};$col=($col | age);
            $mother=$col.Directory.Fullname            
                switch ($file){
                    {$_.startswith("YYYYMMDD")}{$_.replace('YYYYMMDD',$jdate) | %{$_ -replace "\?","[0-9]"} | tee -Variable feed;break}
                    {$_ -contains("YYYYMMDD")}{$_.replace('YYYYMMDD',$jdate) | %{$_ -replace "\?","[0-9]"} | tee -Variable feed;break}
                    {$_.endswith("YYYYMMDD")}{$_.replace('YYYYMMDD',$jdate) | %{$_ -replace "\?","[0-9]"} | tee -Variable feed;break}
                    {$_ -like "*YYYYMMDD*"}{$_.replace('YYYYMMDD',$jdate) | %{$_ -replace "\?","[0-9]"} | tee -Variable feed;break}
                    {$_.startswith("YYYY-MM-DD")}{$_.replace('YYYY-MM-DD',$jdate) | %{$_ -replace "\?","[0-9]"} | tee -Variable feed;break}
                    {$_ -contains("YYYY-MM-DD")}{$_.replace('YYYY-MM-DD',$jdate) | %{$_ -replace "\?","[0-9]"} | tee -Variable feed;break}
                    {$_.endswith("YYYY-MM-DD")}{$_.replace('YYYY-MM-DD',$ddate) | %{$_ -replace "\?","[0-9]"} | tee -Variable feed;break}
                    {$_ -like "*YYYY-MM-DD*"}{$_.replace('YYYY-MM-DD',$ddate) | %{$_ -replace "\?","[0-9]"} | tee -Variable feed;break}
                        }
                    #if ($col.count -gt 1){
                    switch ($col){
                        {$col.count -gt 1}{Do
                            {switch ($col){
                                {$_.Name -match $feed}{$found=$_.Name;$ffile=$_.fullname;$size=$_.Length;
                                    logthis "Bytesize: $size | File: $ffile - Validated!";
                                        $global:class='Validate';break}#Get-Increment;set-logpath;break}
                                default {logthis "Validation failed! File not found - $ffile";$count++;$count;
                                            Msgbox "Validation failed! File not found - $file"
                                                $global:class='failed';#Get-Increment;set-logpath;$count=$null;break;
                                                }
                                            }
                                        }until ($found -match $feed -or $count -eq $col.count)
                                            ;Get-Increment;set-logpath;$count=$null;break;
                                }
                        {!($col)}{logthis "Validation failed! File not found - $file";   
                                Msgbox "Validation failed! File not found - $file"
                                    $global:class='failed';$count=$null;break}
                        default{$col=gci ($source | Split-Path -Parent) | ?{$_.extension -like "*.$type"}| ?{$_.mode -like '-a---'};
                                switch ($col){
                                    {$_.Name -match $feed}{$found=$_.Name;$ffile=$_.fullname;$size=$_.Length;
                                        logthis "Bytesize: $size | File: $ffile - Validated!";
                                            $global:class='Validate';Get-Increment;set-logpath;break}
                                    default {logthis "Validation failed! File not found - $file";
                                        Msgbox "Validation failed! File not found - $file"
                                            $global:class='failed';Get-Increment;set-logpath;break}
                                            }
                                    }
                            }
                    }
            }
    }
    Else {$x=Test-Path ($source)
    $col=gci ($source) | ?{$_.mode -like '-a---'} | ?{$_.extension -like "*.$type"} | tee -Variable item;$item;#$total=$item.count
    $col=$item | ?{$_ -like "*$jdate*"};$total=$col.count
    switch($col){
        {$col.count -eq $n}{logthis "Validation successful! - Source:$source - File Count: $total / Expected: $n"
            $global:class='note';Get-Increment;set-logpath;break
                }
        {!($col)}{logthis "Validation failed! No file(s) found";
            $global:class='failed';$count=$null;;Get-Increment;set-logpath;break}
        default{logthis "Validation Error! - Source:$source - Incorrect number of file(s) found - File Count: $total / Expected: $n"
            Msgbox "Incorrect number of file(s) found - File Count: $total / Expected: $n"
                $global:class='failed';Get-Increment;set-logpath;break}
                }
    switch($x){
        'False'{"Validation Error! Source path not found! - Source:$source";$global:class='failed';
            Msgbox 'Source path not found!'
                Get-Increment;set-logpath;break}
         default{
            $col | % -Begin {$ii=0} -Process {$x=$_; $ii=$ii+1;$record=$col.count
                Write-Progress -Activity "$action File(s)" -status "Processing $ii of $record $type file(s)" -PercentComplete ($ii/$col.count*100) `
                -CurrentOperation "=> $x";logthis "$_ <=>";
                    $global:class='Report';Get-Increment;set-logpath
                    $ojdate=(get-date).AddDays(-1).ToString('yyyyMMdd');$oddate=(get-date).tostring("yyyy-MM-dd")
                }
            }
        }
    }
$command=$myconnection.CreateCommand()
$run="INSERT INTO opslog (taskfile,lpath,file,status,bytesize,seq) VALUES ('"+$logfile+"','"+$fhome+"','"+$found+"','"+$Global:class+"','"+$size+"','"+$global:inc+"')"
$command.CommandText=$run
$reader=$Command.ExecuteReader()
$myconnection.Close()
}
#------------------------- Action script -------------------------#
    
    If ((!($a))){
            Write-Host No action specified!
                    msgbox 'No action specified!'
    }
    Else{

        switch ($a){
            clone {[Parameter(mandatory=$true,Position=0
                )]
                [string]$action
                [Parameter(mandatory=$true,Position=1
                )]
                [string]$source
                [Parameter(mandatory=$true,Position=2
                )]
                [string]$dest
                [Parameter(mandatory=$true,Position=3
                )]
                [string]$type
                [Parameter(mandatory=$false,Position=4
                )]
                [int]$age
                #$dest=$null
                    switch($source){
                        {$_.endswith("YYYYMMDD")}{$source=$source.replace('YYYYMMDD',$jdate)}
                        {$_.startswith("YYYYMMDD")}{$source=$source.replace('YYYYMMDD',$jdate)}
                        {$_ -like "*YYYYMMDD*"}{$source=$source.replace('YYYYMMDD',$jdate)}
                        {$_.endswith("YYYY-MM-DD")}{$source=$source.replace('YYYY-MM-DD',$ddate)}
                        {$_.startswith("YYYY-MM-DD")}{$source=$source.replace('YYYY-MM-DD',$ddate)}
                        {$_ -like "*YYYY-MM-DD*"}{$source=$source.replace('YYYY-MM-DD',$ddate)}
                            Default{$source=$source}    
                                }

                    switch($dest){
                        {$_.endswith("YYYYMMDD")}{$dest=$dest.replace('YYYYMMDD',$jdate)}
                        {$_.startswith("YYYYMMDD")}{$dest=$dest.replace('YYYYMMDD',$jdate)}
                        {$_ -like "*YYYYMMDD*"}{$dest=$dest.replace('YYYYMMDD',$jdate)}
                        {$_.endswith("YYYY-MM-DD")}{$dest=$dest.replace('YYYY-MM-DD',$ddate)}
                        {$_.startswith("YYYY-MM-DD")}{$dest=$dest.replace('YYYY-MM-DD',$ddate)}
                        {$_ -like "*YYYY-MM-DD*"}{$dest=$dest.replace('YYYY-MM-DD',$ddate)}
                            Default{$dest=$dest}
                                }
                    Write-Host Copying File; OPS-Copy; break
                }
            append {[Parameter(mandatory=$true,Position=0
                )]
                [string]$action
                [Parameter(mandatory=$true,Position=1
                )]
                [string]$source
                [Parameter(mandatory=$true,Position=2
                )]
                [string]$dest
                [Parameter(mandatory=$false,Position=3
                )]
                [string]$type
                [Parameter(mandatory=$false,Position=4
                )]
                [int]$age
                    Write-Host Appending to File A; OPS-Append; break
                }
            merge {[Parameter(mandatory=$true,Position=0
                )]
                [string]$action
                [Parameter(mandatory=$true,Position=1
                )]
                [string]$source
                [Parameter(mandatory=$true,Position=2
                )]
                [string]$dest
                [Parameter(mandatory=$false,Position=3
                )]
                [string]$type
                [Parameter(mandatory=$false,Position=4
                )]
                [int]$age
                    Write-Host Merging to single file; OPS-15022-Merge; break
                }
            Rename {[Parameter(mandatory=$true,Position=0
                )]
                [string]$action
                [Parameter(mandatory=$true,Position=1
                )]
                [string]$source
                [Parameter(mandatory=$true,Position=2
                )]
                [string]$dest
                [Parameter(mandatory=$true,Position=3
                )]
                [string]$type
                [Parameter(mandatory=$false,Position=4
                )]
                [int]$age
                    Write-Host Renaming File; OPS-Rename; break
                }
            shift {[Parameter(mandatory=$true,Position=0
                )]
                [string]$action
                [Parameter(mandatory=$true,Position=1
                )]
                [string]$source
                [Parameter(mandatory=$true,Position=2
                )]
                [string]$dest
                [Parameter(mandatory=$true,Position=3
                )]
                [string]$type
                [Parameter(mandatory=$false,Position=4
                )]
                [int]$age
                    Write-Host Moving File; OPS-Move; break
                }
            tidy {[Parameter(mandatory=$true,Position=0
                )]
                [string]$action
                [Parameter(mandatory=$true,Position=1
                )]
                [string]$source
                [Parameter(mandatory=$true,Position=2
                )]
                [string]$type
                [Parameter(mandatory=$true,Position=3
                )]
                [int]$age
                    Write-Host Deleting File;OPS-Tidy; break #$type=$args[2]; OPS-Tidy; break
                }
            zip {[Parameter(mandatory=$true,Position=0
                )]
                [string]$action
                [Parameter(mandatory=$true,Position=1
                )]
                [string]$source
                [Parameter(mandatory=$true,Position=2
                )]
                [string]$dest
                [Parameter(mandatory=$true,Position=3
                )]
                [string]$type
                [Parameter(mandatory=$false,Position=4
                )]
                [int]$age
                    write-host Compressing File;#$archive=($dest).replace('YYYYMMDD',$Jdate);$archive=($dest).replace('YYYY-MM-DD',$ddate);#OPS-Zip;break
                        Get-Increment
                        switch ($global:outfile){
                            y{switch($dest){
                                {$_.endswith("YYYYMMDD")}{$archive=($dest+$global:inc).replace('YYYYMMDD',$jdate)}
                                {$_.startswith("YYYYMMDD")}{$archive=($dest+$global:inc).replace('YYYYMMDD',$jdate)}
                                {$_ -like "*YYYYMMDD*"}{$archive=($dest+$global:inc).replace('YYYYMMDD',$jdate)}
                                {$_.endswith("YYYY-MM-DD")}{$archive=($dest+$global:inc).replace('YYYY-MM-DD',$ddate)}
                                {$_.startswith("YYYY-MM-DD")}{$archive=($dest+$global:inc).replace('YYYY-MM-DD',$ddate)}
                                {$_ -like "*YYYY-MM-DD*"}{$archive=($dest+$global:inc).replace('YYYY-MM-DD',$ddate)}
                                    }
                                }
                            default{switch($dest){
                                {$_.endswith("YYYYMMDD")}{$archive=($dest).replace('YYYYMMDD',$jdate)}
                                {$_.startswith("YYYYMMDD")}{$archive=($dest).replace('YYYYMMDD',$jdate)}
                                {$_ -like "*YYYYMMDD*"}{$archive=($dest).replace('YYYYMMDD',$jdate)}
                                {$_.endswith("YYYY-MM-DD")}{$archive=($dest).replace('YYYY-MM-DD',$ddate)}
                                {$_.startswith("YYYY-MM-DD")}{$archive=($dest).replace('YYYY-MM-DD',$ddate)}
                                {$_ -like "*YYYY-MM-DD*"}{$archive=($dest).replace('YYYY-MM-DD',$ddate)}
								
								{$_ -like "*YYYY-MM_*"}{$archive=($dest).replace('YYYY-MM',$ddateMM)}
								
                                default{$archive=$dest}
                                    }
                                }        
                            }
                    OPS-Zip;break
                }
            unzip {[Parameter(mandatory=$true,Position=0
                )]
                [string]$action
                [Parameter(mandatory=$true,Position=1
                )]
                [string]$source
                [Parameter(mandatory=$true,Position=2
                )]
                [string]$dest
                [Parameter(mandatory=$true,Position=3
                )]
                [string]$type
                [Parameter(mandatory=$false,Position=4
                )]
                [int]$age
                    write-host Compressing File;#$archive=($dest).replace('YYYYMMDD',$Jdate);$archive=($dest).replace('YYYY-MM-DD',$ddate);#OPS-Zip;break
                        Get-Increment
                        switch ($global:outfile){
                            y{switch($source){
                                {$_.endswith("YYYYMMDD")}{$archive=$source.replace('YYYYMMDD',$jdate)}
                                {$_.startswith("YYYYMMDD")}{$archive=$source.replace('YYYYMMDD',$jdate)}
                                {$_ -like "*YYYYMMDD*"}{$archive=$source.replace('YYYYMMDD',$jdate)}
                                {$_.endswith("YYYY-MM-DD")}{$archive=$source.replace('YYYY-MM-DD',$ddate)}
                                {$_.startswith("YYYY-MM-DD")}{$archive=$source.replace('YYYY-MM-DD',$ddate)}
                                {$_ -like "*YYYY-MM-DD*"}{$archive=$source.replace('YYYY-MM-DD',$ddate)}
                                    }
                                }
                            default{switch($source){
                                {$_.endswith("YYYYMMDD")}{$archive=$source.replace('YYYYMMDD',$jdate)}
                                {$_.startswith("YYYYMMDD")}{$archive=$source.replace('YYYYMMDD',$jdate)}
                                {$_ -like "*YYYYMMDD*"}{$archive=$source.replace('YYYYMMDD',$jdate)}
                                {$_.endswith("YYYY-MM-DD")}{$archive=$source.replace('YYYY-MM-DD',$ddate)}
                                {$_.startswith("YYYY-MM-DD")}{$archive=$source.replace('YYYY-MM-DD',$ddate)}
                                {$_ -like "*YYYY-MM-DD*"}{$archive=$source.replace('YYYY-MM-DD',$ddate)}
                                default{$archive=$source}
                                    }
                                }        
                            }
                    OPS-Unzip;break
                }
            val {[Parameter(mandatory=$true,Position=0
                )]
                [string]$action
                [Parameter(mandatory=$true,Position=1
                )]
                [string]$source
                [Parameter(mandatory=$true,Position=2
                )]
                [string]$type
                [Parameter(mandatory=$false,Position=4
                )]
                [int]$age
                [parameter(
                mandatory=$false
                )]
                [int]$n
                    write-host Validating Files;OPS-Val;break
                }
            Default {#[System.Windows.Forms.MessageBox]::Show("$action $stop",'Error: Command not found '+"$logfile")
                    Write-Host Action specified is not valid! Please review Options
                    $global:label='Error Command not found';#write-host $label
                    $alert='y'
                    $global:class='failed'
                        msgbox "Action specified is not valid! `r `n Please review options:`r
    [   clone    ] - Copy file(s) from 1 location to another`n
    [  merge   ] - Grab's multiple files and sets content to 1 output file `n
    [    shift    ] - Moves file(s) from 1 location to another `n
    [  rename ] - Rename's source file `n
    [ append  ] - Appends content of source to output file `n
    [     tidy    ] - Deletes file(s) from source location `n
    [      zip     ] - Compresses file(s) from source location
    [      Val     ] - Validate file(s) from source location"
                            logthis "[$action] action not found, please review options | Taskfile: $logfile"
            }
    }
 }
#$source | ?{$_ -like '*?'}

$Fname
$logpath
$item
#$global:source
#$global:dest
#gci (split-path -parent $global:source)
#$global:fileinc
"Files selectec >> $item"
#$mval
#$source | gm
#$jdate
$valerie
"this is=$valerie2"
#Get-Increment
#Set-Logpath

exit $LASTEXITCODE